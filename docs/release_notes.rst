Release Notes
*************

*Abril 8, 2020*

Detalles de Release 1.5.4
=========================

Bug fixes
---------
- Solucionado error en la instalación al hacer la actualización al release-1.5.3
- Solucionado error en el proceso de modificación de una calificación de gestión que generaba error en el reporte de calificaciones de la campaña
- Solucionado error en la exportación a csv del reporte de calificaciones de una campaña que mostraba mas de una calificación para una misma llamada
